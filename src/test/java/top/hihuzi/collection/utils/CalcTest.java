package top.hihuzi.collection.utils;

import junit.framework.TestCase;
import org.junit.Test;

import static top.hihuzi.collection.utils.Calc.castSafe;

public class CalcTest extends TestCase {
    @Test
    public void testCastSafe() {
        System.out.println(castSafe(-12.34, null));
        System.out.println(castSafe("-12.34", null));
        System.out.println(castSafe("   ", null));
        System.out.println(castSafe("", null));
        System.out.println(castSafe(".1", null));
        System.out.println(castSafe(.1, null));
        System.out.println(castSafe("zg", null));
        System.out.println(castSafe(null, null));
        System.out.println(castSafe(10_00, null));
    }
}