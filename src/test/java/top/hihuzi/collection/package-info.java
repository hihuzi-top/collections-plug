package top.hihuzi.collection;

/*
一个密钥对允许你对你要发布的组件进行GPG签名和验证，可以通过以下命令来生成新的密钥对：
gpg –gen-key
查看本机包含的密钥对
gpg --list-keys
上传公钥到服务器
gpg --keyserver 校验地址 --recv-keys 公钥
校验地址如下
gpg –-keyserver hkv://pool.sks-keyservers.net:11371 –-recv-keys F94A0F67715DD50F54DFCB1A8A0FC2E27AD9AA72
gpg –-keyserver hkv://keyserver.ubuntu.com:11371 –-recv-keys F94A0F67715DD50F54DFCB1A8A0FC2E27AD9AA72

gpg –-keyserver hkp://keyserver.ubuntu.com:11371 –-recv-keys 0BCAE7CEE1C0E7F1D96CD6B8C3D3ECE36EA4CC0C


gpg --keyserver keys.gnupg.net --send-keys F94A0F67715DD50F54DFCB1A8A0FC2E27AD9AA72

gpg --keyserver keys.openpgp.org --send-keys F94A0F67715DD50F54DFCB1A8A0FC2E27AD9AA72

gpg --keyserver pool.sks-keyservers.net --send-keys F94A0F67715DD50F54DFCB1A8A0FC2E27AD9AA72

gpg --keyserver keyserver.ubuntu.com --send-keys F94A0F67715DD50F54DFCB1A8A0FC2E27AD9AA72
*/

